import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Home from "./Views/View1.js";
import  About from "./Views/view2.js";
import  Contact  from "./Views/view3.js";
import MenuListComposition from './components/AddMenu'
import Button from '@material-ui/core/Button';

var styles={
'nav':{    
  background: '#EAF0F1',
  height: '37px'
},
  'link':{ 
    textDecoration: 'none' 
  },
  'flex':{
    display:'flex'
  },

}

function App() {
  return (
    <Router>
    <div>
      <nav style={styles.nav} className="navbar navbar-expand-lg navbar-light bg-light">
              <div style={styles.flex}>
                    <Link style={styles.link} to={'/'}>
                      <Button variant="outlined">View 1</Button>
                  </Link>
                    <Link style={styles.link} to={'/view2'} >
                      <Button variant="outlined">View 2</Button>
                      </Link>
                    <Link style={styles.link} to={'/view3'} >
                      <Button variant="outlined">View 3</Button>
                      </Link>
                   <MenuListComposition />
               </div>
      </nav>
      <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/view2' component={About} />
          <Route path='/view3' component={Contact} />
      </Switch>
    </div>
  </Router>
  );
}

export default App;
