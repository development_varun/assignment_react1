import React from 'react';

class Home extends React.Component {


    render() {
        var styles={
            'root':{
                      height: '100vh',
                      display: 'flex',
                      justifyContent: 'center', 
                      backgroundColor:'#333945'
            },
            'sub':{
                    color: 'whitesmoke' ,
                    letterSpacing: '18px',
                    textTransform: 'uppercase',
                    margin: 'auto'
            }
        }
       return (
          <div style={styles.root}>
             <h1 style={styles.sub}>View 1 ....</h1>
          </div>
       )
    }
 }
 export default Home;