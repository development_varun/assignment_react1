import React from 'react';

class Contact extends React.Component {
 
    render() {
        var styles={
            'root':{
                     height: '100vh',
                     display: 'flex',
                     justifyContent: 'center', 
                     backgroundColor:'#616C6F'
            },
            'sub':{
                  color: 'whitesmoke' ,
                  letterSpacing: '18px',
                  textTransform: 'uppercase',
                  margin: 'auto'
            }       
        }
       return (
          <div  style={styles.root}>
             <h1 style={styles.sub}>View 3 ....</h1>
          </div>
       )
    }
 }
 export default Contact;